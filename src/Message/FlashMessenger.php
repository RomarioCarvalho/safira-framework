<?php

namespace Safira\Message;

use Safira\Session\Session;

class FlashMessenger {

    protected static $session;

    public static function addSuccessMessage($title, $msg) {
        if (self::$session === null) {
            self::$session = new Session;
        }
        
        self::$session->addMessage('success', $title, $msg);
    }
    
    public static function addWarningMessage($title, $msg) {
        if (self::$session === null) {
            self::$session = new Session;
        }
        
        self::$session->addMessage('warning', $title, $msg);
    }

    public static function addErrorMessage($title, $msg) {
        if (self::$session === null) {
            self::$session = new Session;
        }
        
        self::$session->addMessage('danger', $title, $msg);
    }

    public static function addInfoMessage($title, $msg) {
        if (self::$session === null) {
            self::$session = new Session;
        }
        
        self::$session->addMessage('info', $title, $msg);
    }

    public static function hasMessage($name) {
        if (isset($_SESSION[$name])) {
            return true;
        } else {
            return false;
        }
    }

    public static function getMessage($name) {
        $msg = '';
        if ($_SESSION[$name]) {
            $msg = $_SESSION[$name];
            unset($_SESSION[$name]);
        }
        return $msg;
    }

}
