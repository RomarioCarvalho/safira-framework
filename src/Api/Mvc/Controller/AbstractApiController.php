<?php

namespace Safira\Api\Mvc\Controller;

use Safira\Routing\Router;
use Safira\Session\Session;
use Safira\Helpers\Util;
use Safira\Http\HttpRequest;
use Safira\Api\Mvc\Model\RetornoApi;
use Safira\Api\Mvc\Controller\AuthApiController;

/**
 * Classe AbstractApiController
 */
abstract class AbstractApiController {

    /**
     * Variável que possui o objeto do HttpRequest
     * 
     * @var \stdClass 
     */
    private $request;

    /**
     * Variável que possui o objeto do Service atual
     * 
     * @var \stdClass 
     */
    private $entityService;

    /**
     * Variável que possui o objeto da entidade Router
     * 
     * @var \stdClass 
     */
    private $router;

    /**
     * Construtor do AbstractApiController
     */
    public function __construct() {
        $this->router = new Router;
        $this->session = new Session;
    }

    public function getAuthorization() {
        $headers = getallheaders();
        if(!isset($headers["Authorization"])) {
            RetornoApi::erro("A Authorization não foi especificada na solicitação", 401);
        }

        return $headers["Authorization"];
    }

    public function getAccept() {
        $headers = getallheaders();
        if($headers["Accept"] != "application/json") {
            RetornoApi::erro("O Accept enviado no Headers é inválido ou não foi informado", 400);
        }

        return $headers["Accept"];
    }

    public function isRequestPreFlight() {
        if($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
            RetornoApi::sucesso("OK", 200);
        }
    }

    /**
     * 
     * @return \Safira\Api\AbstractApiService
     */
    protected function getEntityService() {
        if ($this->entityService == null) {
            $serviceName = "\app\Services\\" . ucfirst($this->getRouter()->getCurrentController()) . "Service";
            $this->entityService = new $serviceName;
        }

        return $this->entityService;
    }

    /**
     * Retorna o objeto do Service do controller passado por parâmetro
     * @param string $serviceName
     * @return Object Service
     */
    protected function getServiceLocator($serviceName) {
        $serviceName = "\app\Services\\" . ucfirst($serviceName) . "Service";
        $this->serviceLocator = new $serviceName;

        return $this->serviceLocator;
    }

    /**
     * Retorna o objeto da entidade Router
     * @return \Safira\Routing\Router
     */
    protected function getRouter() {
        return $this->router;
    }

    /**
     * 
     * @return \Safira\Http\HttpRequest
     */
    public function getRequest() {
        if (!$this->request) {
            $this->request = new HttpRequest();
        }

        return $this->request;
    }

    public function validAuthorization() {
        $this->isRequestPreFlight();
        $auth = new AuthApiController();
        $auth->validAuthorizationAction();
    }
    
    public function apiPost() {
        $this->isRequestPreFlight();
        if(!$this->getRequest()->isPost()) {
            RetornoApi::erro("O método de solicitação deve ser POST", 405);
        }
    }

    public function apiGet() {
        $this->isRequestPreFlight();
        if(!$this->getRequest()->isGet()) {
            RetornoApi::erro("O método de solicitação deve ser GET", 405);
        }
    }
    

}
