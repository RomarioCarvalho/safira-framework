<?php

namespace Safira\Api\Mvc\Controller;

use Safira\Routing\Router;
use Safira\Session\Session;
use Safira\Helpers\Util;
use Safira\Http\HttpRequest;
use Safira\Api\Mvc\Model;
use Safira\Api\Mvc\Model\RetornoApi;

/**
 * Classe AuthApiController
 */
class AuthApiController extends AbstractApiController {

    /**
     * Variável que possui o objeto do DBInstance do Doctrine
     * 
     * @var \stdClass 
     */
    private $dbInstance;

    public function __construct() {
        $this->dbInstance = include '../config/database.php';
    }

    public function generateTokenAction() {
        $this->isRequestPreFlight();

        $request = $this->getRequest();
        if ($request->isPost()) {
            if(!isset($request->getPost()["grant_type"])) {
                RetornoApi::erro("O grant type não foi especificado na solicitação", 400);
            }
    
            if(!isset($request->getPost()["user_id"]) || !isset($request->getPost()["password"])) {
                RetornoApi::erro("O user_id e/ou password não foram especificados na solicitação", 400);
            }
    
            $authorizarion = explode(" ", $this->getAuthorization());
            if($authorizarion[0] == "Basic") {
                $authBase64 = explode(":", base64_decode($authorizarion[1]));
                $username =  $authBase64[0];
                $pass =  $authBase64[1];
    
                $sql = "SELECT id_oauth_client FROM oauth_client WHERE username = '{$username}' AND password = '{$pass}'";
                $query = $this->getDBInstance()->query($sql);
                
                $row = $query->fetch();
                
                if($row) {
                    $idClient = $row["id_oauth_client"];
                } else {
                    RetornoApi::erro("A Authorization informada no Headers é inválida", 400);    
                }
            } else {
                RetornoApi::erro("A Authorization informada no Headers é inválida", 400);
            }
    
            $idUsuario = (int)base64_decode($request->getPost()["user_id"]);
            $password = sha1(base64_decode($request->getPost()["password"]));
            
            $sql = "SELECT id_usuario FROM usuario WHERE id_usuario = {$idUsuario} AND senha = '{$password}'";
            $query = $this->getDBInstance()->query($sql);
            
            $row = $query->fetch();
    
            if($row) {
                $date = new \DateTime();
                $now = $date->format('Y-m-d H:i:s');
                $expiresObj = $date->add(new \DateInterval('P10D'));
                $expiracao = $expiresObj->format('Y-m-d H:i:s');
                $token = sha1($now);
                
                $sql = "INSERT INTO oauth_token (id_usuario, id_oauth_client, access_token, data_expiracao) VALUES ({$idUsuario}, {$idClient}, '{$token}', '{$expiracao}')";
                $this->getDBInstance()->query($sql);
                
                $obj = new \stdClass();
                $obj->access_token = $token;
                $obj->expires = $expiracao;
    
                RetornoApi::sucesso("OK", $obj);
                
            } else {
                RetornoApi::erro("As credenciais do cliente estão inválidas", 401);
            }
        } else {
            RetornoApi::erro("O método de solicitação deve ser POST ao solicitar um token de acesso", 405);
        }
        
    }

    public function revokeAction() {
        $request = $this->getRequest();
        
        if ($request->isPost()) {
            if(!isset($request->getPost()["token"])) {
                RetornoApi::erro("O token não foi especificado na solicitação", 400);
            }

            $token = $request->getPost()["token"];
            $sql = "DELETE FROM oauth_token WHERE access_token = '{$token}';";
            $this->getDBInstance()->query($sql);
            
            RetornoApi::sucesso("OK");
        } else {
            RetornoApi::erro("O método de revogação de token deve ser POST", 405);
        }
    }

    public function validAuthorizationAction() {
        $authorizarion = explode(" ", $this->getAuthorization());
        if($authorizarion[0] == "Bearer") {
            $token = $authorizarion[1];
            $sql = "SELECT data_expiracao FROM OAuth_Token WHERE access_token = '{$token}'; ";
            $query = $this->getDBInstance()->query($sql);
            $row = $query->fetch();
                
            if($row) {
                $expiracao = new \DateTime($row["data_expiracao"]);
                $date = new \DateTime();

                if($expiracao < $date) {
                    RetornoApi::erro("Token expirado", 401);
                }
            } else {
                RetornoApi::erro("A Authorization informada no Headers é inválida", 400);
            }
        } else {
            RetornoApi::erro("O token type informado no Headers é inválido", 400);
        }
    }

    private function getDBInstance() {
        return $this->dbInstance;
    }

}
