<?php

namespace Safira\Api\Mvc\Model;

class RetornoApi {

    public $status;
    public $codigo;
    public $mensagem;
    public $dados;

    public static function sucesso($msg, $dados = null) {
        $retornoAPI = new RetornoAPI();
        $retornoAPI->status = true;
        $retornoAPI->codigo = 200;
        $retornoAPI->mensagem = $msg;
        $retornoAPI->dados = $dados;

        header("Content-Type: application/json");
        header("Status: 200");
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Methods: PUT, GET, POST, PATCH, DELETE, OPTIONS");
        header("Access-Control-Allow-Headers: Authorization, Origin, X-Requested-With, Content-Type, Accept");
        http_response_code(200);
        echo json_encode($retornoAPI);
        die();
    }

    public static function erro($msg, $codigo = 400, $dados = null) {
        $retornoAPI = new RetornoAPI();
        $retornoAPI->status = false;
        $retornoAPI->codigo = $codigo;
        $retornoAPI->mensagem = $msg;
        $retornoAPI->dados = $dados;

        header("Content-Type: application/json");
        header("Status: {$codigo}");
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Methods: PUT, GET, POST, PATCH, DELETE, OPTIONS");
        header("Access-Control-Allow-Headers: Authorization, Origin, X-Requested-With, Content-Type, Accept");
        http_response_code($codigo);
        echo json_encode($retornoAPI);
        die();
    }

    public static function erroPadrao() {
        $retornoAPI = new RetornoAPI();
        $retornoAPI->status = false;
        $retornoAPI->codigo = 400;
        $retornoAPI->mensagem = "Desculpe, ocorreu um problema na sua solicitação!";

        header("Content-Type: application/json");
        header("Status: 400");
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Methods: PUT, GET, POST, PATCH, DELETE, OPTIONS");
        header("Access-Control-Allow-Headers: Authorization, Origin, X-Requested-With, Content-Type, Accept");
        http_response_code(400);
        echo json_encode($retornoAPI);
        die();
    }

    
}
