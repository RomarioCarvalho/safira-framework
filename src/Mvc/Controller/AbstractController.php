<?php

namespace Safira\Mvc\Controller;

use Safira\Routing\Router;
use Safira\Session\Session;
use Safira\Helpers\Util;
use Safira\Message\FlashMessenger;
use Safira\Http\HttpRequest;

/**
 * Classe AbstractController
 */
abstract class AbstractController {

    /**
     * Variável com o nome da action atual
     * @var string 
     */
    private $action;
    
    /**
     * Variável que possui o objeto da entidade SessionManager
     * 
     * @var \stdClass 
     */
    private $session;
    
    /**
     * Variável que possui o objeto da entidade Router
     * 
     * @var \stdClass 
     */
    private $router;
    
    /**
     * Todas as variáveis que estarão disponíveis na View
     * @var array 
     */
    private $variables = array();
    
    /**
     * Variável que possui o objeto do Service atual
     * 
     * @var \stdClass 
     */
    private $entityService;
    
    /**
     * Variável que possui o objeto do Service a ser definido
     * 
     * @var \stdClass 
     */
    private $serviceLocator;
    
    /**
     * Variável que possui os campos obrigatórios
     * 
     * @var \stdClass 
     */
    private $fieldsRequired;
    
    /**
     * Variável que possui os campos inválidos
     * 
     * @var \stdClass 
     */
    private $fieldsInvalid = array();
    
    /**
     * Variável que possui o objeto do HttpRequest
     * 
     * @var \stdClass 
     */
    private $request;
    
    /**
     * Variável com o template da página a ser exibida 
     * @var string 
     */
    private $template;
    
    /**
     * Variável com o título da página a ser exibida 
     * @var string 
     */
    protected $titlePage;

    /**
     * Construtor do AbstractController
     */
    public function __construct() {
        $this->router = new Router;
        $this->session = new Session;
        $this->setVariable('controller', $this->getRouter()->getCurrentController());
        $this->setVariable('action', $this->getRouter()->getCurrentAction());
        $this->setVariable('scripts', $this->getScriptsArray());
        $this->setVariable('styles', $this->getStylesArray());
        $this->setTemplate('default');
    }

    /**
     * Renderiza a view
     * @param string $action
     * Exemplo: index, add, edit, delete
     */
    protected function render($action) {
        if($this->hasPermission()) {
            $this->action = $action;

            if ($this->hasTemplate()) {
                if (count($this->variables) > 0) {
                    extract($this->variables, EXTR_OVERWRITE);
                }
                include_once "../app/Views/Templates/{$this->getTemplate()}.phtml";
            } else {
                $this->content();
            }
        } else {
            FlashMessenger::addErrorMessage($this->titlePage, Util::message('noPermission'));
            $this->getRouter()->goToRoot();
        }
    }
    
    /**
     * Complemento do render() que carrega a view caso não tenha um template definido
     */
    public function content() {
        $className = str_replace('app\\Controllers\\', '', get_class($this));
        $singleClassName = preg_replace('/Controller/', '', $className, 1);

        if (count($this->variables) > 0) {
            extract($this->variables, EXTR_OVERWRITE);
        }

        include_once '../app/Views/' . $singleClassName . '/' . $this->action . '.phtml';
    }
    
    /**
     * Verifica se o usuário tem acesso ao controler ou action atual
     * @return boolean
     */
    public function hasPermission() {
        return true;
    }

    /**
     * Popula o objeto da entidade com os dados da View
     * @param Model $entity
     * @return Model $entity
     */
    protected function bind($entity) {
        $attrClass = $this->getEntityService()->getAttributesClass();
        $request = $this->getRequest();
        
        foreach ($attrClass as $attr) {
            $att = strtolower($attr);
            if (isset($request->getPost()[$attr])) {
                $entity->$att = $request->getPost($attr);
            }
        }
        
        return $entity;
    }

    /**
     * Valida os dados da View
     * @param Model $entity
     * @return boolean
     */
    protected function isValid($entity) {
        if ($this->getFieldsRequired()) {
            foreach ($this->getFieldsRequired() as $key => $value) {
                $att = strtolower($key);
                if ($entity->$att == "" || $entity->$att == null) {
                    $this->setFieldsInvalid($value);
                }
            }
        }

        if ($this->getFieldsInvalid()) {
            return false;
        }

        return true;
    }

    /**
     * Modifica o objeto query builder, aplicando os filtros provinientes do
     * formulário.
     * 
     * @param \Doctrine\ORM\QueryBuilder $qb
     */
    public function applyFormFilter($qb) {
        if (is_subclass_of($this->getModelName(), 'Safira\Mvc\Model\AbstractModel')) {
            $qb->where('excluido', '=', 0);
        }
        
        return $qb;
    }
    
    /**
     * @return string Representa o namespace da classe Model atual
     */
    public function getModelName() {
        return $this->getEntityService()->getModelName();
    }
    
    /**
     * @return string Representa o nome simplificado da classe Model atual
     */
    public function getModelSingleName() {
        return $this->getEntityService()->getModelSingleName();
    }

    /**
     * @return string Representa uma instância da classe Model atual
     */
    protected function getModelInstance() {
        $modelName = $this->getModelName();
        return new $modelName;
    }

    /**
     * Define as variáveis que serão usadas na View
     *
     * @param  string $name
     * @param  mixed $value
     */
    protected function setVariable($name, $value) {
        $this->variables[(string) $name] = $value;
    }

    /**
     * Retorna o objeto da entidade Router
     * @return \Safira\Routing\Router
     */
    protected function getRouter() {
        return $this->router;
    }

    /**
     * Retorna o objeto da entidade Session
     * @return \Safira\Session\Session
     */
    protected function getSession() {
        return $this->session;
    }

    /**
     * 
     * @return \Safira\Http\HttpRequest
     */
    public function getRequest() {
        if (!$this->request) {
            $this->request = new HttpRequest();
        }

        return $this->request;
    }

    /**
     * 
     * @return \Safira\Mvc\Service\AbstractService
     */
    protected function getEntityService() {
        if ($this->entityService == null) {
            $serviceName = "\app\Services\\" . ucfirst($this->getRouter()->getCurrentController()) . "Service";
            $this->entityService = new $serviceName;
        }

        return $this->entityService;
    }

    /**
     * Retorna o objeto do Service do controller passado por parâmetro
     * @param string $serviceName
     * @return Object Service
     */
    protected function getServiceLocator($serviceName) {
        $serviceName = "\app\Services\\" . ucfirst($serviceName) . "Service";
        $this->serviceLocator = new $serviceName;

        return $this->serviceLocator;
    }

    /**
     * Este método retorna os javascripts que deverão estar nas páginas das ações
     * padrão ('index', 'add', 'edit' e 'delete') 
     * @return array
     */
    protected function getScriptsArray() {
        return array();
    }

    /**
     * Este método retorna as folhas de estilo (CSS) que deverão estar nas 
     * páginas das ações padrão ('index', 'add', 'edit' e 'delete') 
     * @return array
     */
    protected function getStylesArray() {
        return array();
    }

    /**
     * Retorna o template que será usuado
     * @return string
     */
    function getTemplate() {
        return $this->template;
    }

    /**
     * Define o template que será usuado
     * @param string $template
     */
    protected function setTemplate($template) {
        $this->template = $template;
    }

    /**
     * Verifica se há um template definido para a ação
     * @return boolean
     */
    protected function hasTemplate() {
        if (!is_null($this->getTemplate()) && file_exists("../app/Views/Templates/{$this->getTemplate()}.phtml")) {
            return true;
        }

        return false;
    }

    /**
     * Retorna quais campos são obrigatórios no formulário
     * @return array
     */
    protected function getFieldsRequired() {
        return $this->fieldsRequired;
    }

    /**
     * Define quais campos são obrigatórios no formulário
     * @param array $fieldsRequired
     */
    protected function setFieldsRequired($fieldsRequired) {
        $this->fieldsRequired = $fieldsRequired;
    }

    /**
     * Retorna quais campos estão inválidos no formulário
     * @return array
     */
    protected function getFieldsInvalid() {
        return $this->fieldsInvalid;
    }

    /**
     * Define quais campos estão inválidos no formulário
     * @return array
     */
    private function setFieldsInvalid($fieldsInvalid) {
        $this->fieldsInvalid[] = $fieldsInvalid;
    }

    /**
     * Retorna o título da página a ser exibida
     * @return string
     */
    public function getTitlePage() {
        return $this->titlePage;
    }

    /**
     * Define o título da página a ser exibida
     * @param string $titlePage
     */
    public function setTitlePage($titlePage) {
        $this->titlePage = $titlePage;
    }

}
