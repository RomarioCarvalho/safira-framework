<?php

namespace Safira\Mvc\Service;

use Exception;

abstract class AbstractService {

    /**
     * Variável que possui o objeto do DBInstance do Doctrine
     * 
     * @var \stdClass 
     */
    private $dbInstance;

    /**
     * Construtor do AbstractService
     */
    public function __construct() {
        $dbInstance = include '../config/database.php';
        $this->setDBInstance($dbInstance);
    }

    /**
     * 
     * @return \FaaPz\PDO\Database
     */
    public function getDBInstance() {
        return $this->dbInstance;
    }

    private function setDBInstance($dbInstance) {
        $this->dbInstance = $dbInstance;
    }

    /**
     * Retorna uma instância do Model utilizado
     */
    public function getModelInstance() {
        $modelName = $this->getModelName();
        return new $modelName;
    }

    /**
     * @return string Representa no nome da classe Model, daquele controller
     */
    abstract public function getModelName();

    /**
     * @return string Representa no nome da classe Model, daquele controller
     */
    public function getModelSingleName() {
        $modelName = $this->getModelName();
        $modelName = explode('\\', $modelName);
        return $modelName[2];
    }
    
    /**
     * Busca os dados da entidade no banco de dados
     * @param int $id
     * @param Model $model
     * @return Model
     */
    public function find($id = null, $model = null) {
        try {
            if (!$model) {
                $model = $this->getModelSingleName();
            }

            $query = $this->getDBInstance()
                    ->select()
                    ->from($model)
                    /*->where('ativo', '=', 1)*/;

            if (is_null($id)) {
                $stmt = $query->execute();
                $list = array();
                while ($obj = $stmt->fetchObject($this->getModelName())) {
                    $list[] = $obj;
                }
                return $list;
            } else {
                $query->where($this->getNameIdEntityDB(), '=', $id);
                $stmt = $query->execute();
                return $stmt->fetchObject($this->getModelName());
            }

        } catch (Exception $exc) {
            die("Error: " . $exc->getMessage());
            throw new Exception($exc->getMessage());
        }
    }

    /**
     * Insere os dados da entidade no banco de dados
     * @param array $fieldsDB - Ex: array('column1_db', 'column1_db')
     * @throws Exception
     */
    public function create($fieldsDB) {
        try {
            $model = $this->getModelSingleName();

            $query = $this->getDBInstance()
                    ->insert($fieldsDB)
                    ->into($model);

            $query->execute(false);
        } catch (Exception $exc) {
            throw new Exception($exc->getMessage());
        }
    }

    /**
     * Atualiza os dados da entidade no banco de dados
     * @param int $id
     * @param array $values - Ex: array('column_db' => 'new_value')
     */
    public function update($id, $values) {
        try {
            $model = $this->getModelSingleName();

            $query = $this->getDBInstance()
                    ->update($values)
                    ->table($model)
                    ->where($this->getNameIdEntityDB(), '=', $id);

            $query->execute();
        } catch (Exception $exc) {
            throw new Exception($exc->getMessage());
        }
    }

    /**
     * Remove (logicamente ou não) os dados da entidade no banco de dados
     * @param Model $entity
     */
    public function delete($entity) {
        try {
            $model = $this->getModelSingleName();

            if ($entity instanceof \Safira\Mvc\Model\AbstractModel) {
                $query = $this->getDBInstance()
                        ->update(array('excluido' => 1))
                        ->table($model)
                        ->where($this->getNameIdEntityDB(), '=', $entity->getId());
            } else {
                $query = $this->getDBInstance()
                        ->delete()
                        ->from($model)
                        ->where($this->getNameIdEntityDB(), '=', $entity->getId());
            }

            $query->execute();
        } catch (Exception $exc) {
            throw new Exception($exc->getMessage());
        }
    }

    /**
     * Retorna todos os atributos da classe atual
     * @return array
     */
    public function getAttributesClass() {
        $method = new \ReflectionClass($this->getModelName());

        $class = $method->getProperties();
        foreach ($class as $prop) {
            $attributes[] = $prop->getName();
        }

        if ($method->getParentClass()) {
            $parentClass = $method->getParentClass()->getProperties();
            foreach ($parentClass as $prop) {
                $attributes[] = $prop->getName();
            }
        }

        return $attributes;
    }
 
    /**
     * Retorna o nome da PK da classe atual
     * @return string
     */
    public function getNameIdEntityDB() {
        return 'id_' . strtolower($this->getModelSingleName());
    }

    /**
     * Retorna todos os valores da entidade atual em formato de array
     * @param Model $entity
     * @return array
     */
    public function getValuesEntity($entity) {
        $attrClass = $this->getAttributesClass();
        $values = array();
        foreach ($attrClass as $attr) {
            $attr = strtolower($attr);
            $values[] = $entity->$attr;
        }

        return $values;
    }

    public function getQueryBuilder() {
        $model = $this->getModelSingleName();

        $query = $this->getDBInstance()
                ->select()
                ->from($model);

        return $query;
    }

    public function getCountResult($qb) {
        $stmt = $qb->execute();
        $fetchAll = $stmt->fetchAll();

        return count($fetchAll);
    }

}
