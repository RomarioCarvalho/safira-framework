<?php

namespace Safira\Mvc\Model;

/**
 * Description of AbstractModel
 */
abstract class AbstractModel {

    /**
     * @var smallint
     */
    private $ativo = 1;

    public function __get($name) {
        $att = strtolower($name);
        if (!property_exists($this, $att)) {
            $className = get_class($this);
            throw new \Exception("O atributo '{$att}' não existe na classe '{$className}'");
        }
        return $this->{$att};
    }

    public function __set($name, $value) {
        $att = strtolower($name);
        if (!property_exists($this, $att)) {
            $className = get_class($this);
            throw new \Exception("O atributo '{$att}' não existe na classe '{$className}'");
        }
        return $this->{$att} = $value;
    }
    
}
