<?php

namespace Safira\Init;

use Safira\Http\PageError;
use Safira\Api\Mvc\Model\RetornoApi;

abstract class Bootstrap {

    private $url;
    private $explode;
    protected $controller;
    protected $action;
    protected $params;
    private $pageError;

    public function __construct() {
        $this->setUrl();
        $this->setExplode();
        $this->setController();
        $this->setAction();
        $this->init();
        $this->run();
    }

    abstract protected function init();

    private function setUrl() {
        $_GET['url'] = (!empty($_GET['url']) ? $_GET['url'] : 'index/index');
        $this->url = $_GET['url'];
    }

    private function setExplode() {
        $this->explode = explode('/', $this->url);
    }

    private function setController() {
        if($this->explode[0] == "api") {
            $this->controller = $this->explode[1];
        } else {
            $this->controller = $this->explode[0];
        }
    }

    public function getController() {
        return $this->controller;
    }

    protected function setAction() {
        if($this->explode[0] == "api") {
            if(!isset($this->explode[2])) {
                $ac = 'generateTokenAction';
            } else {
                $ac = (!isset($this->explode[2]) || $this->explode[2] == null || $this->explode[2] == "index" ? "indexAction" : $this->explode[2] . 'Action');
            }
        } else {
            $ac = (!isset($this->explode[1]) || $this->explode[1] == null || $this->explode[1] == "index" ? "indexAction" : $this->explode[1] . 'Action');
        }
        $this->action = $ac;
    }
    
    public function getAction() {
        return $this->action;
    }

    public function run() {
        if($this->explode[0] == "api") {
            if($this->controller == "oauth") {
                $class = "Safira\\Api\\Mvc\\Controller\\AuthApiController";
            } else {
                $class = "app\\Controllers\\Api\\" . ucfirst($this->controller) . 'Controller';
            }
        } else {
            $class = "app\\Controllers\\" . ucfirst($this->controller) . 'Controller';
        }

        if (class_exists($class)) {
            $controller = new $class;
            $action = $this->action;
            if (method_exists($controller, $action)) {
                $controller->$action();
            } else {
                if($this->explode[0] == "api") {
                    RetornoApi::erro("O método especificado na solicitação não foi encontrado", 404);
                } else {
                    $this->getPageError()->goPage(404);
                }
            }
        } else {
            if($this->explode[0] == "api") {
                RetornoApi::erro("A classe especificada na solicitação não foi encontrada", 404);
            } else {
                $this->getPageError()->goPage(404);
            }
        }
    }

    public function getPageError() {
        return new PageError();
    }

}
