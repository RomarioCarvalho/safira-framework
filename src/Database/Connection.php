<?php

namespace Safira\Database;

use FaaPz\PDO\Database as Database;
use Exception;

class Connection {

    private static $instance = NULL;
    
    /**
     * 
     * @return \FaaPz\PDO\Database
     */
    public static function getInstance($config) {
        try {
            if (!isset(self::$instance)) {
                self::$instance = new Database($config['driver'] . ':host=' . $config['host'] . ';dbname=' . $config['database'], $config['username'], $config['password']);
            }
            return self::$instance;
            
        } catch (Exception $exc) {
            die("Error Database: " . $exc->getMessage());
        }

    }

}
