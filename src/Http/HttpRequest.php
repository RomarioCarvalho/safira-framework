<?php

namespace Safira\Http;

class HttpRequest {

    public function isPost() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            return true;
        }

        return false;
    }

    public function getPost($field = null) {
        if ($field) {
            if($this->isPost()) {
                return $_POST[$field];
            }
            
            return false;
        }

        return $_POST;
    }

    public function isGet() {
        if ($_SERVER['REQUEST_METHOD'] === 'GET') {
            return true;
        }

        return false;
    }

    public function getGet($field = null) {
        if ($field) {
            if($this->isGet()) {
                return $_GET[$field];
            }
            
            return false;
        }

        return $_GET;
    }

}
