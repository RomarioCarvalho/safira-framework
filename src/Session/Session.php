<?php

namespace Safira\Session;

class Session {

    public function setSession($name, $value) {
        $_SESSION[$name] = $value;
        return $this;
    }

    public function getSession($name) {
        if(isset($_SESSION[$name])) {
            return $_SESSION[$name];
        }
        return false;
    }

    public function addMessage($type, $title, $message) {
        $_SESSION['msg'] = array(
            'type' => $type,
            'title' => $title,
            'message' => $message
        );
        return $this;
    }

    public function destroySession($name) {
        unset($_SESSION[$name]);
        return $this;
    }

    public function hasSession($name) {
        return isset($_SESSION[$name]);
    }
    
    public function userLogged() {
        return $this->getSession('userLogged');
    }
    
    public function getUserInfo($key = null) {
        $data = $this->getSession('userInfo');

        if ($key) {
            return $data[$key];
        }

        return $data;
    }

}
