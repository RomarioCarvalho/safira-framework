<?php

namespace Safira\Helpers;

class Util {

    public static function message($string, $field = array()) {
        $json = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/message.json'));
        
        $msg = "";

        if ($field) {
            if (isset($json->$string)) {
                foreach ($field as $f) {
                    $msg .= "<li>" . str_replace('$field', $f, $json->$string) . "</li>";
                }
            } else {
                $msg = $string;
            }
        } else {
            if (isset($json->$string)) {
                $msg = $json->$string;
            } else {
                $msg = $string;
            }
        }

        return $msg;
    }

    public static function convertToDate($date, $format = 'd/m/Y') {
        $date = strtotime($date);
        return date($format, $date);
    }

    public static function convertToMoney($value, $decimal = 2) {
        return number_format($value, $decimal, ',', '.');
    }

    public static function mask($tipo = "", $string, $size = 10) {
        $string = preg_replace("[^0-9]", "", $string);
        switch ($tipo) {
            case 'fone':
                if($size === 10){
                    $string = '(' . substr($string, 0, 2) . ') ' . substr($string, 2, 4) . '-' . substr($string, 6);
                } else if($size === 11){
                    $string = '(' . substr($string, 0, 2) . ') ' . substr($string, 2, 5) . '-' . substr($string, 7);
                }
                break;
            case 'cep':
                $string = substr($string, 0, 5) . '-' . substr($string, 5, 3);
                break;
            case 'cpf':
                $string = substr($string, 0, 3) . '.' . substr($string, 3, 3) . '.' . substr($string, 6, 3) . '-' . substr($string, 9, 2);
                break;
            case 'cnpj':
                $string = substr($string, 0, 2) . '.' . substr($string, 2, 3) . '.' . substr($string, 5, 3) . '/' . substr($string, 8, 4) . '-' . substr($string, 12, 2);
                break;
            case 'rg':
                $string = substr($string, 0, 2) . '.' . substr($string, 2, 3) . '.' . substr($string, 5, 3);
                break;
            default:
                $string = 'É ncessário definir um tipo(fone, cep, cpg, cnpj, rg)';
                break;
        }
        return $string;
    }

    public static function bandeiraCartaoCredito($cartao) {
        $cartao = preg_replace("/[^0-9]/", "", $cartao);

        switch ($cartao) {
            case (bool) preg_match('/^(40117[8-9]|431274|438935|451416|457393|45763[1-2]|506(699|7[0-6][0-9]|77[0-8])|509\d{3}|504175|506699|627780|636297|636368|636369|65003[1-3]|6500(3[5-9]|4[0-9]|5[0-1])|6504(0[5-9]|[1-3][0-9])|650(4[8-9][0-9]|5[0-2][0-9]|53[0-8])|6505(4[1-9]|[5-8][0-9]|9[0-8])|6507(0[0-9]|1[0-8])|65072[0-7]|6509(0[1-9]|1[0-9]|20)|6516(5[2-9]|[6-7][0-9])|6550([0-1][0-9]|2[1-9]|[3-4][0-9]|5[0-8]))/', $cartao) :
                $bandeira = 'elo';
                break;

            case (bool) preg_match('/^(637095|637599|637609|637612|637600|637568)/', $cartao) :
                $bandeira = 'hiper';
                break;
            
            case (bool) preg_match('/^6(?:011|5[0-9]{2})[0-9]{12}$/', $cartao) :
                $bandeira = 'discover';
                break;

            case (bool) preg_match('/^3(0[0-5]|[68]\d)\d{11}$/', $cartao) :
                $bandeira = 'diners';
                break;

            case (bool) preg_match('/^3[47]\d{13}$/', $cartao) :
                $bandeira = 'amex';
                break;

            case (bool) preg_match('/^(?:2131|1800|35\d{3})\d{11}$/', $cartao) :
                $bandeira = 'jcb';
                break;

            case (bool) preg_match('/^(606282\d{10}(\d{3})?)|(3841\d{15})$/', $cartao) :
                $bandeira = 'hipercard';
                break;

            case (bool) preg_match('/^(5078\d{2})(\d{2})(\d{11})$/', $cartao) :
                $bandeira = 'aura';
                break;

            case (bool) preg_match('/^(?:5[0678]\d\d|6304|6390|67\d\d)\d{8,15}$/', $cartao) :
                $bandeira = 'maestro';
                break;

            case (bool) preg_match('/^4\d{12}(\d{3})?$/', $cartao) :
                $bandeira = 'visa';
                break;

            case (bool) preg_match('/^(5[1-5]\d{4}|677189)\d{10}$/', $cartao) :
                $bandeira = 'mastercard';
                break;
            default :
                $bandeira = "";
                break;
        }

        return $bandeira;
    }

    public static function removerCaracteresEspeciais($string) {
        return str_replace("(", "", str_replace(")", "", str_replace("-", "", str_replace(" ", "", str_replace("/", "", str_replace("*", "", str_replace(".", "", str_replace("!", "", str_replace("@", "", str_replace("#", "", str_replace("$", "", str_replace("%", "", str_replace("&", "", str_replace("+", "", str_replace(",", "", $string)))))))))))))));
    }

    public static function removerAcentos($string) {
        return trim(preg_replace('~[^0-9a-z]+~i', '-', preg_replace('~&([a-z]{1,2})(acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i', '$1', htmlentities($string, ENT_QUOTES, 'UTF-8'))), ' ');
    }

    public static function truncateWord($string, $width, $break = " ...") {
        $string = trim($string);
        if (strlen($string) > $width) {
            $tmpstr = substr($string, 0, $width) . $break;
            return $tmpstr;
        } else {
            return $string;
        }
    }

    public static function formatarHoraAtendimento($hora) {
        $horaArr = explode(":", $hora);
        $horaVal = $horaArr[0] . "h";
        if(substr($horaArr[1], 0, 1) != "0") {
            $horaVal = $horaArr[0] . "h" . $horaArr[1];
        }

        return $horaVal;
    }

    public static function montarNumeroContaBancaria($banco, $conta) {
        $zeros = '';
        $conta = str_replace("-", "", $conta);
        $qtdCaracteres = strlen($conta);
        
        switch ($banco) {
            case 1:
            case 2:
                $digito = substr($conta, -1);
                $conta = substr($conta, 0, $qtdCaracteres-1);
                
                for ($index = 0; $index < 9 - $qtdCaracteres; $index++) {
                    $zeros .= "0";
                }
                
                return $zeros . $conta . "-" . $digito;
                break;
            case 3:
                $digito = substr($conta, -1);
                $operacao = substr($conta, 0, 3);
                $posOperacao = substr($conta, 3, $qtdCaracteres);
                $qdtPosOperacao = strlen($posOperacao);
                $conta = substr($posOperacao, 0, $qdtPosOperacao-1);

                for ($index = 0; $index < 9 - $qdtPosOperacao; $index++) {
                    $zeros .= "0";
                }
         
                return $operacao . $zeros . $conta . "-" . $digito;
                break;
            case 4:
                $digito = substr($conta, -1);
                $conta = substr($conta, 0, $qtdCaracteres-1);
                
                for ($index = 0; $index < 8 - $qtdCaracteres; $index++) {
                    $zeros .= "0";
                }
                
                return $zeros . $conta . "-" . $digito;
                break;
            case 5:
                $digito = substr($conta, -1);
                $conta = substr($conta, 0, $qtdCaracteres-1);
                
                for ($index = 0; $index < 6 - $qtdCaracteres; $index++) {
                    $zeros .= "0";
                }
                
                return $zeros . $conta . "-" . $digito;
                break;
            case 6:
            case 8:
            case 9:
            case 10:
                $digito = substr($conta, -1);
                $conta = substr($conta, 0, $qtdCaracteres-1);
                
                for ($index = 0; $index < 10 - $qtdCaracteres; $index++) {
                    $zeros .= "0";
                }
                
                return $zeros . $conta . "-" . $digito;
                break;
            case 7:
                for ($index = 0; $index < 6 - $qtdCaracteres; $index++) {
                    $zeros .= "0";
                }
                
                return $zeros . $conta;
                break;
            default:
                break;
        }
    }

    public static function montarCodigoAtivacao($length = 6) {
        $characters = '0123456789';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public static function uploadFotoPerfil($imagem, $idUsuario) {
        $extensoes = array('png', 'jpg', 'jpeg');
        $uploadDir = 'public/img-usuarios/';
        
        $type = explode('/', $imagem['type']);
        $ext = $type[1];
        
        if(in_array($ext, $extensoes)) {
            $fileName = $uploadDir . basename(sha1($idUsuario)) . '.png';
            move_uploaded_file($imagem["tmp_name"], $fileName);
        }
        
    }

    public static function is_mobile() {
        $useragent = $_SERVER['HTTP_USER_AGENT'];
        if (preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od|ad)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i', $useragent) || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($useragent, 0, 4))) {
            return true;
        }

        return false;
    }
    
    public static function resumoTexto($str, $num) {

        $str = trim($str);
        $str = preg_replace('/&nbsp;/', '', $str, 1);

        if (strlen($str) > $num) { // Se a string for maior que o número a ser cropado
            // Corta string na quantidade dada
            $tmpstr = substr($str, 0, $num);
            $excerpt = explode(' ', $tmpstr); // Gera array com as palavras

            array_pop($excerpt); // Tiro o último item do array
            $excerpt = implode(" ", $excerpt) . " [...]";
            echo $excerpt;
        } else {
            echo $str;
        }
    }

}
